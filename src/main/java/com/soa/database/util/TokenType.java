package com.soa.database.util;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;

/**
 * 
 *<b>json转换为实体的types</b>
 *<p>
 * 用于保存常用Type
 *</p>
 */
public class TokenType {
	/** 列表map */
	public static Type LIST_MAP = new TypeToken<List<Map<String, String>>>() {
	}.getType();
}
