package com.soa.database.util;

/**
 * 
 * 
 *<b>json转换类的工具</b>
 *<p>
 * 提供了jsonnameAS转换
 *</p>
 */
public class JsonHelper {
	/**
	 * 
	 *<b>构建json key 转换</b>
	 * 
	 * @param oldName
	 *@param newName
	 *@return JsonFieldNameAs
	 */
	public static JsonFieldNameAs nameAs(String oldName, String newName) {
		return new JsonFieldNameAs(oldName, newName);
	}

	/**
	 * 
	 *<b>构建json key 转换 的数组</b>
	 * 
	 * @param fieldNameAs
	 *@return JsonFieldNameAs
	 */
	public static JsonFieldNameAs[] fieldName(JsonFieldNameAs... fieldNameAs) {
		return fieldNameAs;
	}

	/**
	 * 
	 *<b> JSON key转换 </b>
	 *<p>
	 * json key转换实体
	 *</p>
	 */
	public static class JsonFieldNameAs {
		/** 原来的名字 */
		private String oldName;

		/** 新名字 */
		private String newName;

		public JsonFieldNameAs(String oldName, String newName) {
			this.oldName = oldName;
			this.newName = newName;
		}

		public JsonFieldNameAs(String oldName) {
			this.oldName = oldName;
		}

		public String getOldName() {
			return oldName;
		}

		public void setOldName(String oldName) {
			this.oldName = oldName;
		}

		public String getNewName() {
			return newName;
		}

		public void setNewName(String newName) {
			this.newName = newName;
		}

	}
}
