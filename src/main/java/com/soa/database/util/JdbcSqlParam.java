package com.soa.database.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 *<b>数据库命令占位符参数</b>
 *<p>
 * sql动态拼接,用于接收参数表达式的实体
 *</p>
 */
public class JdbcSqlParam {
	/** sql语句 */
	private String sql;
	/** 参数集合 */
	private List<Map<String, Object>> keys = new ArrayList<Map<String, Object>>();
	/** 具体值 */
	private Object[] args;

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public List<Map<String, Object>> getKeys() {
		return keys;
	}

	public void setKeys(List<Map<String, Object>> keys) {
		this.keys = keys;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

}
