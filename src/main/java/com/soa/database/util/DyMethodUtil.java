package com.soa.database.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.springframework.util.StringUtils;




public class DyMethodUtil {
    public static Object invokeMethod(String jexlExp,Map<String,Object> map){  
    	JexlEngine jexl=new JexlEngine(); 
    	Expression e = jexl.createExpression(jexlExp);
    	JexlContext jexlContext = new MapContext();
    	for (Entry<String,Object> entry:map.entrySet()) {
    		jexlContext.set(entry.getKey(), entry.getValue());
		}
    	return e.evaluate(jexlContext);
    } 
    
    public static boolean isempt(Object string){
    	if(StringUtils.isEmpty(string)){
    		return true;
    	}
    	return false;
    }
    public static void main(String[] args) {
    	
    	String sql="\"a1\".length()";
    	
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("a", null);
		System.out.println(invokeMethod(sql,map));
	}
    public static HashMap<String, Object> convertToMap(Object obj)
            throws Exception {

        HashMap<String, Object> map = new HashMap<String, Object>();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (int i = 0, len = fields.length; i < len; i++) {
            String varName = fields[i].getName();
            boolean accessFlag = fields[i].isAccessible();
            fields[i].setAccessible(true);

            Object o = fields[i].get(obj);
            if (o != null)
                map.put(varName, o.toString());

            fields[i].setAccessible(accessFlag);
        }

        return map;
    }
}
