package com.soa.database.util;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;


public class Pagination extends JdbcDaoSupport {
	public static final int NUMBERS_PER_PAGE = 10;
	// 一页显示的记录数
	private int numPerPage;
	// 记录总数
	private int totalRows;
	// 总页数
	private int totalPages;
	// 当前页码
	private int currentPage;
	// 起始行数
	private int startIndex;
	// 结束行数
	private int lastIndex;
	// 结果集存放List
	private List<Map<String, Object>> resultList;
	// JdbcTemplate jTemplate
	private JdbcTemplate jTemplate;

	/**
	 * 每页显示10条记录的构造函数,使用该函数必须先给Pagination设置currentPage，jTemplate初值
	 * 
	 * @param sql
	 *            oracle语句
	 */
	public Pagination(String sql,String type,Object[] params) {
		if (jTemplate == null) {
			throw new IllegalArgumentException(
					"com.deity.ranking.util.Pagination.jTemplate is null,please initial it first. ");
		} else if (sql.equals("")) {
			throw new IllegalArgumentException(
					"com.deity.ranking.util.Pagination.sql is empty,please initial it first. ");
		}
		new Pagination(sql,type,params, currentPage, NUMBERS_PER_PAGE, jTemplate);
	}

	/**
	 * 分页构造函数 　　
	 * 
	 * * @param sql 根据传入的sql语句得到一些基本分页信息 　
	 * 　* @param currentPage 当前页 　　* @param  * @param type 数据库类型
	 * numPerPage 每页记录数 　　* @param jTemplate JdbcTemplate实例
	 */
	public Pagination(String sql,String type,Object[] params ,int currentPage, int numPerPage,
			JdbcTemplate jTemplate) {
		if (jTemplate == null) {
			throw new IllegalArgumentException(
					"com.deity.ranking.util.Pagination.jTemplate is null,please initial it first. ");
		} else if (sql == null || sql.equals("")) {
			throw new IllegalArgumentException(
					"com.deity.ranking.util.Pagination.sql is empty,please initial it first. ");
		}
		// 设置每页显示记录数
		setNumPerPage(numPerPage);
		// 设置要显示的页数
		setCurrentPage(currentPage);
		// 计算总记录数
		StringBuffer totalSQL = new StringBuffer(" SELECT count(*) FROM ( ");
		totalSQL.append(sql);
		totalSQL.append(" ) totalTable ");
		// 给JdbcTemplate赋值
		setJdbcTemplate(jTemplate);
		int totalRows=getJdbcTemplate().queryForObject(totalSQL.toString(),params,Integer.class);
		// 总记录数
		setTotalRows(totalRows);
		// 计算总页数
		setTotalPages();
		// 计算起始行数
		setStartIndex();
		// 计算结束行数
		setLastIndex();
		// 构造oracle数据库的分页语句
		String pagesql=JdbcUtil.getPageSQL(sql, startIndex, lastIndex, type);

		// 装入结果集
		setResultList(getJdbcTemplate().queryForList(pagesql,params));
	}
	
	/**
	 * 分页构造函数 　　
	 *  @param sql 根据传入的sql语句得到一些基本分页信息 　
 	 *	@param currentPage 当前页 　　
 	 * @param type 数据库类型
	 * numPerPage 每页记录数 　　* @param jTemplate JdbcTemplate实例
	 */
	public Pagination(String sql,String type ,int currentPage, int numPerPage,
			Session session) {
		if (sql == null || sql.equals("")) {
			throw new IllegalArgumentException(
					"com.deity.ranking.util.Pagination.sql is empty,please initial it first. ");
		}
		// 设置每页显示记录数
		setNumPerPage(numPerPage);
		// 设置要显示的页数
		setCurrentPage(currentPage);
		// 计算总记录数
		StringBuffer totalSQL = new StringBuffer(" SELECT count(*) num FROM ( ");
		totalSQL.append(sql);
		totalSQL.append(" ) totalTable ");
		// 总记录数
		setTotalRows(((BigDecimal)session.createSQLQuery(totalSQL.toString()).uniqueResult()).intValue());
		// 计算总页数
		setTotalPages();
		// 计算起始行数
		setStartIndex();
		// 计算结束行数
		setLastIndex();
		// 构造oracle数据库的分页语句
		String pagesql=JdbcUtil.getPageSQL(sql, startIndex, lastIndex, type);
		// 装入结果集
		setResultList(session.createSQLQuery(pagesql).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list());
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getNumPerPage() {
		return numPerPage;
	}

	public void setNumPerPage(int numPerPage) {
		this.numPerPage = numPerPage;
	}

	public List<Map<String, Object>> getResultList() {
		return resultList;
	}

	public void setResultList(List<Map<String, Object>> resultList) {
		this.resultList = resultList;
	}

	public int getTotalPages() {
		return totalPages;
	}

	// 计算总页数
	public void setTotalPages() {
		if (totalRows % numPerPage == 0) {
			this.totalPages = totalRows / numPerPage;
		} else {
			this.totalPages = (totalRows / numPerPage) + 1;
		}
	}

	public int getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex() {
		this.startIndex = (currentPage - 1) * numPerPage;
	}

	public int getLastIndex() {
		return lastIndex;
	}

	public JdbcTemplate getJTemplate() {
		return jTemplate;
	}

	public void setJTemplate(JdbcTemplate template) {
		jTemplate = template;
	}

	// 计算结束时候的索引
	public void setLastIndex() {
		if (totalRows < numPerPage) {
			this.lastIndex = totalRows;
		} else if ((totalRows % numPerPage == 0)
				|| (totalRows % numPerPage != 0 && currentPage < totalPages)) {
			this.lastIndex = currentPage * numPerPage;
		} else if (totalRows % numPerPage != 0 && currentPage == totalPages) {// 最后一页
			this.lastIndex = totalRows;
		}
	}
	
	
	public  String getJson(){
		return Json.toJson(this);
	}
	
}
