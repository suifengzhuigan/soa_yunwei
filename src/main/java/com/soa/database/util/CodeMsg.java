package com.soa.database.util;

import java.util.ArrayList;

public class CodeMsg {
	
	private int resultcode;
	private String resultmessage;
	public Object response;
	
	//通用的错误码
	public static CodeMsg SUCCESS = new CodeMsg(10000, "处理成功");
	public static CodeMsg ERROR_KEY = new CodeMsg(10001, "错误的请求apikey");
	public static CodeMsg NONE_KEY = new CodeMsg(10004, "URL上apikey参数不能为空,且服务不允许匿名调用");
	public static CodeMsg MUST_AUTH = new CodeMsg(10010, "接口需要申请，按照流程提交资料");
	public static CodeMsg SERVICE_NONE = new CodeMsg(10011, "服务未找到");
	public static CodeMsg SYS_BUSY = new CodeMsg(10020, "系统繁忙，请稍后再试");
	public static CodeMsg GATEWAY_ERROR = new CodeMsg(10030, "网关调用失败，请联系管理员");
	public static CodeMsg TIME_OUT = new CodeMsg(10031, "第三方接口调用超时");
	public static CodeMsg SERVICE_BUSSNESS = new CodeMsg(10032, "当前时间内调用过于频繁，请稍后调用");
	public static CodeMsg SERVICE_404 = new CodeMsg(10033, "404错误，找不到页面");
	public static CodeMsg SERVICE_CONNECTEXCEPTION = new CodeMsg(10034, "接口网络连接异常，请联系管理员");
	public static CodeMsg SERVICE_NETFLIX_ERROR = new CodeMsg(10035, "组件注册异常，注册中心未找到服务组件，请联系管理员");
	public static CodeMsg SQL_ERROR = new CodeMsg(10036, "数据库查询异常");
	public static CodeMsg RATE_LIMIT_ERROR = new CodeMsg(10040, "超过每天限量，请明天继续");
	public static CodeMsg LIMIT_ERROR = new CodeMsg(10041, "超过总限量，请重新申请");
	public static CodeMsg LIMIT_ERROR_DATE = new CodeMsg(10042, "服务不在有效期内");
	public static CodeMsg USER_ERROR = new CodeMsg(10050, "用户需要审核，请联系管理员审核");
	public static CodeMsg SERVICE_MERCHANT_ERROR = new CodeMsg(10060, "提供方设置调用权限，请联系提供方");
	public static CodeMsg SERVICE_STATE_ERROR = new CodeMsg(10070, "服务状态禁用，请联系管理员解禁");
	public static CodeMsg API_STATE_ERROR = new CodeMsg(10080, "API规则禁用，请联系管理员解禁");
	public static CodeMsg API_NULL = new CodeMsg(10081, "API规则未找到，请联系管理员");
	public static CodeMsg HITRIX = new CodeMsg(10082, "代理不可用！断路器打开");
	private CodeMsg( ) {
	}
			
	private CodeMsg( int resultcode,String resultmessage ,Object response) {
		this.resultcode = resultcode;
		this.resultmessage = resultmessage;
		this.response=response;
	}
	private CodeMsg( int resultcode,String resultmessage ) {
		this.resultcode = resultcode;
		this.resultmessage = resultmessage;
		this.response=new ArrayList();
	}
	public int getResultcode() {
		return resultcode;
	}
	public void setResultcode(int resultcode) {
		this.resultcode = resultcode;
	}
	public String getResultmessage() {
		return resultmessage;
	}
	public void setResultmessage(String resultmessage) {
		this.resultmessage = resultmessage;
	}
	
	public CodeMsg fillArgs(Object... args) {
		int resultcode = this.resultcode;
		String message = String.format(this.resultmessage, args);
		return new CodeMsg(resultcode, message);
	}
	
	public CodeMsg SuccessMessage() {
		return SUCCESS;
	}

	@Override
	public String toString() {
		return "CodeMsg [code=" + resultcode + ", resultmessage=" + resultmessage + "]";
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}
	
	
}
