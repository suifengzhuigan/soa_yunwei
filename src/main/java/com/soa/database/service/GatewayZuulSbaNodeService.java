package com.soa.database.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.soa.database.entity.GatewayZuulSbaNode;

/**
 * Created by zx on 2018/6/1.
 */
@Service
@Transactional
public class GatewayZuulSbaNodeService {
	@PersistenceContext
	private EntityManager entityManager;

	private Session session;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public Session getSession() {
		return this.getEntityManager().unwrap(org.hibernate.Session.class);
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	public GatewayZuulSbaNode queryByPK(String id){
		GatewayZuulSbaNode gatewayZuulSbaNode =this.getEntityManager().find(GatewayZuulSbaNode.class, id);
		return gatewayZuulSbaNode;
	}
	public void update(GatewayZuulSbaNode gatewayZuulSbaNode){
		this.getSession().update(gatewayZuulSbaNode);
	}
	
	public List<GatewayZuulSbaNode> getGatewayZuulSbaNodeList(Integer page){
		Criteria criteria = this.getSession().createCriteria(GatewayZuulSbaNode.class);
		criteria.addOrder(Order.desc("nodeStatus"));
		return criteria.list();
	}
}
