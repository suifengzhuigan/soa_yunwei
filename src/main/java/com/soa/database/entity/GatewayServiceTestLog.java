package com.soa.database.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * GatewayServiceTestLog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_service_test_log", schema = "public")
public class GatewayServiceTestLog implements java.io.Serializable {

	// Fields

	private String id;
	private GatewayServiceTestGroupLog gatewayServiceTestGroupLog;
	private String serviceId;
	private String serviceName;
	private String testId;
	private String testName;
	private String url;
	private Timestamp operateTime;
	private Timestamp operateEndTime;
	private Integer continueTime;
	private String state;
	private String responseContent;

	// Constructors

	/** default constructor */
	public GatewayServiceTestLog() {
	}

	/** full constructor */
	public GatewayServiceTestLog(
			GatewayServiceTestGroupLog gatewayServiceTestGroupLog,
			String serviceId, String serviceName, String testId,
			String testName, String url, Timestamp operateTime, Timestamp operateEndTime,
			Integer continueTime, String state, String responseContent) {
		this.gatewayServiceTestGroupLog = gatewayServiceTestGroupLog;
		this.serviceId = serviceId;
		this.serviceName = serviceName;
		this.testId = testId;
		this.testName = testName;
		this.url = url;
		this.operateTime = operateTime;
		this.operateEndTime = operateEndTime;
		this.continueTime = continueTime;
		this.state = state;
		this.responseContent = responseContent;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "test_group_log_id")
	@JsonIgnore
	public GatewayServiceTestGroupLog getGatewayServiceTestGroupLog() {
		return this.gatewayServiceTestGroupLog;
	}

	public void setGatewayServiceTestGroupLog(
			GatewayServiceTestGroupLog gatewayServiceTestGroupLog) {
		this.gatewayServiceTestGroupLog = gatewayServiceTestGroupLog;
	}

	@Column(name = "service_id", length = 32)
	public String getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@Column(name = "service_name", length = 300)
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Column(name = "test_id", length = 32)
	public String getTestId() {
		return this.testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	@Column(name = "test_name")
	public String getTestName() {
		return this.testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	@Column(name = "url", length = 300)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "operate_time", length = 19)
	public Timestamp getOperateTime() {
		return this.operateTime;
	}

	public void setOperateTime(Timestamp operateTime) {
		this.operateTime = operateTime;
	}

	@Column(name = "operate_end_time", length = 19)
	public Timestamp getOperateEndTime() {
		return this.operateEndTime;
	}

	public void setOperateEndTime(Timestamp operateEndTime) {
		this.operateEndTime = operateEndTime;
	}

	@Column(name = "continue_time")
	public Integer getContinueTime() {
		return this.continueTime;
	}

	public void setContinueTime(Integer continueTime) {
		this.continueTime = continueTime;
	}

	@Column(name = "state", length = 200)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "response_content", length = 5000)
	public String getResponseContent() {
		return this.responseContent;
	}

	public void setResponseContent(String responseContent) {
		this.responseContent = responseContent;
	}

}