package com.soa.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * GatewayServiceTest entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_service_test", schema = "public")
public class GatewayServiceTest implements java.io.Serializable {

	// Fields

	private String id;
	private String path;
	private String method;
	private String bodyType;
	private String header;
	private String params;
	private String assert1;
	private String assert2;
	private String serviceId;
	private String serviceName;
	private String testGroupId;
	private String testGroupName;
	private String resultCode;
	private String rawType;
	private String assert1Value;
	private String assert2Value;
	private String state;
	private String checkState;
	private String reason;
	// Constructors

	/** default constructor */
	public GatewayServiceTest() {
	}

	/** full constructor */
	public GatewayServiceTest(String path, String method, String bodyType,
			String header, String params, String assert1, String assert2,
			String serviceId, String serviceName, String testGroupId,
			String testGroupName, String resultCode, String rawType,
			String assert1Value, String assert2Value) {
		this.path = path;
		this.method = method;
		this.bodyType = bodyType;
		this.header = header;
		this.params = params;
		this.assert1 = assert1;
		this.assert2 = assert2;
		this.serviceId = serviceId;
		this.serviceName = serviceName;
		this.testGroupId = testGroupId;
		this.testGroupName = testGroupName;
		this.resultCode = resultCode;
		this.rawType = rawType;
		this.assert1Value = assert1Value;
		this.assert2Value = assert2Value;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "path", length = 1000)
	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Column(name = "method", length = 100)
	public String getMethod() {
		return this.method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "body_type")
	public String getBodyType() {
		return this.bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	@Column(name = "header", length = 3000)
	public String getHeader() {
		return this.header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	@Column(name = "params", length = 3000)
	public String getParams() {
		return this.params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	@Column(name = "assert1", length = 1000)
	public String getAssert1() {
		return this.assert1;
	}

	public void setAssert1(String assert1) {
		this.assert1 = assert1;
	}

	@Column(name = "assert2", length = 1000)
	public String getAssert2() {
		return this.assert2;
	}

	public void setAssert2(String assert2) {
		this.assert2 = assert2;
	}

	@Column(name = "service_id", length = 32)
	public String getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@Column(name = "service_name", length = 100)
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Column(name = "test_group_id", length = 32)
	public String getTestGroupId() {
		return this.testGroupId;
	}

	public void setTestGroupId(String testGroupId) {
		this.testGroupId = testGroupId;
	}

	@Column(name = "test_group_name")
	public String getTestGroupName() {
		return this.testGroupName;
	}

	public void setTestGroupName(String testGroupName) {
		this.testGroupName = testGroupName;
	}

	@Column(name = "result_code")
	public String getResultCode() {
		return this.resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	@Column(name = "raw_type")
	public String getRawType() {
		return this.rawType;
	}

	public void setRawType(String rawType) {
		this.rawType = rawType;
	}

	@Column(name = "assert1_value", length = 1000)
	public String getAssert1Value() {
		return this.assert1Value;
	}

	public void setAssert1Value(String assert1Value) {
		this.assert1Value = assert1Value;
	}

	@Column(name = "assert2_value", length = 1000)
	public String getAssert2Value() {
		return this.assert2Value;
	}

	public void setAssert2Value(String assert2Value) {
		this.assert2Value = assert2Value;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	@Column(name = "check_state")
	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState;
	}
	@Column(name = "reason")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}