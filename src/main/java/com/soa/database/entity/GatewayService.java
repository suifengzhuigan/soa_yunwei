package com.soa.database.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



/**
 * GatewayService entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_service", schema = "public")
public class GatewayService implements java.io.Serializable {

	// Fields

	private String id;
	private String serviceName;
	private String serviceDescription;
	private String executeType;
	private String themeId;
	private Integer sorts;
	private String testResultSuccess;
	private String testResultError;
	private String state;
	private Long totalExecuteNum;
	private Integer usedUserNum;
	private String requestDemo;
	private String apiName;
	private String apiId;
	private String content;
	private String published;
	private String path;
	private String isNeedAuth;
	private String isNeedLog;
	private String javaSdk;
	private Timestamp createDate;
	private String resultType;
	private String imageName;
	private String serviceUrl;
	private String sellerContact;
	private String webserviceMethod;
	private String bodyType;
	private String rawType;
	private Set<GatewayServiceParams> gatewayServiceParamses = new HashSet<GatewayServiceParams>(
			0);


	// Constructors

	/** default constructor */
	public GatewayService() {
	}

	/** full constructor */
	public GatewayService(String serviceName, String serviceDescription,
			String executeType, String themeId, Integer sorts,
			String testResultSuccess, String testResultError, String state,
			Long totalExecuteNum, Integer usedUserNum, String requestDemo,
			String apiName, String apiId, String content, String published,
			Set<GatewayServiceParams> gatewayServiceParamses) {
		this.serviceName = serviceName;
		this.serviceDescription = serviceDescription;
		this.executeType = executeType;
		this.themeId = themeId;
		this.sorts = sorts;
		this.testResultSuccess = testResultSuccess;
		this.testResultError = testResultError;
		this.state = state;
		this.totalExecuteNum = totalExecuteNum;
		this.usedUserNum = usedUserNum;
		this.requestDemo = requestDemo;
		this.apiName = apiName;
		this.apiId = apiId;
		this.content = content;
		this.published = published;
		this.gatewayServiceParamses = gatewayServiceParamses;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "service_name")
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Column(name = "service_description", length = 1000)
	public String getServiceDescription() {
		return this.serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	@Column(name = "execute_type", length = 20)
	public String getExecuteType() {
		return this.executeType;
	}

	public void setExecuteType(String executeType) {
		this.executeType = executeType;
	}

	@Column(name = "theme_id", length = 32)
	public String getThemeId() {
		return this.themeId;
	}

	public void setThemeId(String themeId) {
		this.themeId = themeId;
	}

	@Column(name = "sorts")
	public Integer getSorts() {
		return this.sorts;
	}

	public void setSorts(Integer sorts) {
		this.sorts = sorts;
	}

	@Column(name = "test_result_success")
	public String getTestResultSuccess() {
		return this.testResultSuccess;
	}

	public void setTestResultSuccess(String testResultSuccess) {
		this.testResultSuccess = testResultSuccess;
	}

	@Column(name = "test_result_error")
	public String getTestResultError() {
		return this.testResultError;
	}

	public void setTestResultError(String testResultError) {
		this.testResultError = testResultError;
	}

	@Column(name = "state", length = 2)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "total_execute_num")
	public Long getTotalExecuteNum() {
		return this.totalExecuteNum;
	}

	public void setTotalExecuteNum(Long totalExecuteNum) {
		this.totalExecuteNum = totalExecuteNum;
	}

	@Column(name = "used_user_num")
	public Integer getUsedUserNum() {
		return this.usedUserNum;
	}

	public void setUsedUserNum(Integer usedUserNum) {
		this.usedUserNum = usedUserNum;
	}

	@Column(name = "request_demo", length = 2000)
	public String getRequestDemo() {
		return this.requestDemo;
	}

	public void setRequestDemo(String requestDemo) {
		this.requestDemo = requestDemo;
	}

	@Column(name = "api_name")
	public String getApiName() {
		return this.apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	@Column(name = "api_id", length = 32)
	public String getApiId() {
		return this.apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	@Column(name = "content")
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "published", length = 6)
	public String getPublished() {
		return this.published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "gatewayService")
	public Set<GatewayServiceParams> getGatewayServiceParamses() {
		return this.gatewayServiceParamses;
	}

	public void setGatewayServiceParamses(
			Set<GatewayServiceParams> gatewayServiceParamses) {
		this.gatewayServiceParamses = gatewayServiceParamses;
	}


	@Column(name = "is_need_auth")
	public String getIsNeedAuth() {
		return isNeedAuth;
	}

	public void setIsNeedAuth(String isNeedAuth) {
		this.isNeedAuth = isNeedAuth;
	}
	@Column(name = "path")
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Column(name = "is_need_log")
	public String getIsNeedLog() {
		return isNeedLog;
	}

	public void setIsNeedLog(String isNeedLog) {
		this.isNeedLog = isNeedLog;
	}
	@Column(name = "create_date", length = 22)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "java_sdk")
	public String getJavaSdk() {
		return javaSdk;
	}

	public void setJavaSdk(String javaSdk) {
		this.javaSdk = javaSdk;
	}

	@Column(name = "result_type")
	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	@Column(name = "image_name")
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	@Column(name = "service_url")
	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	@Column(name = "seller_contact")
	public String getSellerContact() {
		return sellerContact;
	}

	public void setSellerContact(String sellerContact) {
		this.sellerContact = sellerContact;
	}
	@Column(name = "webservice_method")
	public String getWebserviceMethod() {
		return webserviceMethod;
	}

	public void setWebserviceMethod(String webserviceMethod) {
		this.webserviceMethod = webserviceMethod;
	}
	@Column(name = "body_type")
	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}
	@Column(name = "raw_type")
	public String getRawType() {
		return rawType;
	}

	public void setRawType(String rawType) {
		this.rawType = rawType;
	}
	
}