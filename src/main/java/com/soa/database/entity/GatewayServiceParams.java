package com.soa.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * GatewayServiceParams entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_service_params", schema = "public")
public class GatewayServiceParams implements java.io.Serializable {

	// Fields

	private String id;
	private GatewayService gatewayService;
	private String paramName;
	private String required;
	private String dataType;
	private String memo;
	private String requestOrResponse;
	private Integer sorts;
	private String demo;

	// Constructors

	/** default constructor */
	public GatewayServiceParams() {
	}

	/** full constructor */
	public GatewayServiceParams(GatewayService gatewayService,
			String paramName, String required, String dataType, String memo,
			String requestOrResponse, Integer sorts, String demo) {
		this.gatewayService = gatewayService;
		this.paramName = paramName;
		this.required = required;
		this.dataType = dataType;
		this.memo = memo;
		this.requestOrResponse = requestOrResponse;
		this.sorts = sorts;
		this.demo = demo;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "gateway_service_id")
	public GatewayService getGatewayService() {
		return this.gatewayService;
	}

	public void setGatewayService(GatewayService gatewayService) {
		this.gatewayService = gatewayService;
	}

	@Column(name = "param_name")
	public String getParamName() {
		return this.paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	@Column(name = "required", length = 2)
	public String getRequired() {
		return this.required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	@Column(name = "data_type", length = 20)
	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	@Column(name = "memo")
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Column(name = "request_or_response", length = 10)
	public String getRequestOrResponse() {
		return this.requestOrResponse;
	}

	public void setRequestOrResponse(String requestOrResponse) {
		this.requestOrResponse = requestOrResponse;
	}

	@Column(name = "sorts")
	public Integer getSorts() {
		return this.sorts;
	}

	public void setSorts(Integer sorts) {
		this.sorts = sorts;
	}

	@Column(name = "demo")
	public String getDemo() {
		return this.demo;
	}

	public void setDemo(String demo) {
		this.demo = demo;
	}

}