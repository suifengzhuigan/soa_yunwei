package com.soa.database.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * GatewayServiceTestGroup entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_service_test_group", schema = "public")
public class GatewayServiceTestGroup implements java.io.Serializable {

	// Fields

	private String id;
	private GatewayServiceTestGroup gatewayServiceTestGroup;
	private String groupName;
	private String groupType;
	private String groupDescribe;
	private Integer sorts;
	private String state;
	private String isAlert;
	private String alertEmail;
	private String alertPhone;
	private Integer allCount;
	private Integer errorCount;
	private Integer successCount;
	private Set<GatewayServiceTestGroup> gatewayServiceTestGroups = new HashSet<GatewayServiceTestGroup>(
			0);

	// Constructors

	/** default constructor */
	public GatewayServiceTestGroup() {
	}

	/** full constructor */
	public GatewayServiceTestGroup(
			GatewayServiceTestGroup gatewayServiceTestGroup, String groupName,
			String groupType, String groupDescribe, Integer sorts,
			String state, String isAlert, String alertEmail, String alertPhone,
			Integer allCount, Integer errorCount, Integer successCount,
			Set<GatewayServiceTestGroup> gatewayServiceTestGroups) {
		this.gatewayServiceTestGroup = gatewayServiceTestGroup;
		this.groupName = groupName;
		this.groupType = groupType;
		this.groupDescribe = groupDescribe;
		this.sorts = sorts;
		this.state = state;
		this.isAlert = isAlert;
		this.alertEmail = alertEmail;
		this.alertPhone = alertPhone;
		this.allCount = allCount;
		this.errorCount = errorCount;
		this.successCount = successCount;
		this.gatewayServiceTestGroups = gatewayServiceTestGroups;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent")
	@JsonIgnore
	public GatewayServiceTestGroup getGatewayServiceTestGroup() {
		return this.gatewayServiceTestGroup;
	}

	public void setGatewayServiceTestGroup(
			GatewayServiceTestGroup gatewayServiceTestGroup) {
		this.gatewayServiceTestGroup = gatewayServiceTestGroup;
	}

	@Column(name = "group_name")
	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Column(name = "group_type", length = 2)
	public String getGroupType() {
		return this.groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	@Column(name = "group_describe", length = 1000)
	public String getGroupDescribe() {
		return this.groupDescribe;
	}

	public void setGroupDescribe(String groupDescribe) {
		this.groupDescribe = groupDescribe;
	}

	@Column(name = "sorts")
	public Integer getSorts() {
		return this.sorts;
	}

	public void setSorts(Integer sorts) {
		this.sorts = sorts;
	}

	@Column(name = "state", length = 2)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "is_alert", length = 2)
	public String getIsAlert() {
		return this.isAlert;
	}

	public void setIsAlert(String isAlert) {
		this.isAlert = isAlert;
	}

	@Column(name = "alert_email", length = 2000)
	public String getAlertEmail() {
		return this.alertEmail;
	}

	public void setAlertEmail(String alertEmail) {
		this.alertEmail = alertEmail;
	}

	@Column(name = "alert_phone", length = 2000)
	public String getAlertPhone() {
		return this.alertPhone;
	}

	public void setAlertPhone(String alertPhone) {
		this.alertPhone = alertPhone;
	}

	@Column(name = "all_count")
	public Integer getAllCount() {
		return this.allCount;
	}

	public void setAllCount(Integer allCount) {
		this.allCount = allCount;
	}

	@Column(name = "error_count")
	public Integer getErrorCount() {
		return this.errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

	@Column(name = "success_count")
	public Integer getSuccessCount() {
		return this.successCount;
	}

	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "gatewayServiceTestGroup")
	@JsonIgnore
	public Set<GatewayServiceTestGroup> getGatewayServiceTestGroups() {
		return this.gatewayServiceTestGroups;
	}

	public void setGatewayServiceTestGroups(
			Set<GatewayServiceTestGroup> gatewayServiceTestGroups) {
		this.gatewayServiceTestGroups = gatewayServiceTestGroups;
	}

}