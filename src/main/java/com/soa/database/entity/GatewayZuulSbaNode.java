package com.soa.database.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 * GatewayZuulSbaNode entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_zuul_sba_node",schema = "public")
public class GatewayZuulSbaNode implements java.io.Serializable {

	// Fields

	private String id;
	private String gatewayZuulSbaId;
	private String ip;
	private String port;
	private String username;
	private String password;
	private String shellStart;
	private String shellStop;
	private String shellRestart;
	private String nodeStatus;
	private String nodeName;
	private Date createTime;
	private Date restartTime;
	private String shellStatus;

	// Constructors

	/** default constructor */
	public GatewayZuulSbaNode() {
	}

	/** minimal constructor */
	public GatewayZuulSbaNode(String gatewayZuulSbaId) {
		this.gatewayZuulSbaId = gatewayZuulSbaId;
	}

	/** full constructor */
	public GatewayZuulSbaNode(String gatewayZuulSbaId, String ip, String port,
			String username, String password, String shellStart,
			String shellStop, String shellRestart, String nodeStatus,
			String nodeName, Date createTime, Date restartTime) {
		this.gatewayZuulSbaId = gatewayZuulSbaId;
		this.ip = ip;
		this.port = port;
		this.username = username;
		this.password = password;
		this.shellStart = shellStart;
		this.shellStop = shellStop;
		this.shellRestart = shellRestart;
		this.nodeStatus = nodeStatus;
		this.nodeName = nodeName;
		this.createTime = createTime;
		this.restartTime = restartTime;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "gateway_zuul_sba_id", nullable = false, length = 32)
	public String getGatewayZuulSbaId() {
		return this.gatewayZuulSbaId;
	}

	public void setGatewayZuulSbaId(String gatewayZuulSbaId) {
		this.gatewayZuulSbaId = gatewayZuulSbaId;
	}

	@Column(name = "ip", length = 32)
	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "port", length = 32)
	public String getPort() {
		return this.port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	@Column(name = "username")
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "shell_start")
	public String getShellStart() {
		return this.shellStart;
	}

	public void setShellStart(String shellStart) {
		this.shellStart = shellStart;
	}

	@Column(name = "shell_stop")
	public String getShellStop() {
		return this.shellStop;
	}

	public void setShellStop(String shellStop) {
		this.shellStop = shellStop;
	}

	@Column(name = "shell_restart")
	public String getShellRestart() {
		return this.shellRestart;
	}

	public void setShellRestart(String shellRestart) {
		this.shellRestart = shellRestart;
	}

	@Column(name = "node_status", length = 32)
	public String getNodeStatus() {
		return this.nodeStatus;
	}

	public void setNodeStatus(String nodeStatus) {
		this.nodeStatus = nodeStatus;
	}

	@Column(name = "node_name")
	public String getNodeName() {
		return this.nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", length = 13)
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "restart_time", length = 13)
	public Date getRestartTime() {
		return this.restartTime;
	}

	public void setRestartTime(Date restartTime) {
		this.restartTime = restartTime;
	}
	@Column(name = "shell_status")
	public String getShellStatus() {
		return shellStatus;
	}
	
	public void setShellStatus(String shellStatus) {
		this.shellStatus = shellStatus;
	}

}