package com.soa.database.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * GatewayServiceTestGroupLog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "gateway_service_test_group_log", schema = "public")
public class GatewayServiceTestGroupLog implements java.io.Serializable {

	// Fields

	private String id;
	private String testGroupNum;
	private String groupId;
	private String groupName;
	private Integer testCount;
	private Integer successCount;
	private Integer errorCount;
	private Timestamp operateTime;
	private Timestamp operateEndTime;
	private Integer continueTime;
	private Set<GatewayServiceTestLog> gatewayServiceTestLogs = new HashSet<GatewayServiceTestLog>(
			0);

	// Constructors

	/** default constructor */
	public GatewayServiceTestGroupLog() {
	}

	/** full constructor */
	public GatewayServiceTestGroupLog(String testGroupNum, String groupId,
			String groupName, Integer testCount, Integer successCount,
			Integer errorCount, Timestamp operateTime, Timestamp operateEndTime,
			Integer continueTime,
			Set<GatewayServiceTestLog> gatewayServiceTestLogs) {
		this.testGroupNum = testGroupNum;
		this.groupId = groupId;
		this.groupName = groupName;
		this.testCount = testCount;
		this.successCount = successCount;
		this.errorCount = errorCount;
		this.operateTime = operateTime;
		this.operateEndTime = operateEndTime;
		this.continueTime = continueTime;
		this.gatewayServiceTestLogs = gatewayServiceTestLogs;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "uuid.hex")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false, length = 32)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "test_group_num", length = 20)
	public String getTestGroupNum() {
		return this.testGroupNum;
	}

	public void setTestGroupNum(String testGroupNum) {
		this.testGroupNum = testGroupNum;
	}

	@Column(name = "group_id", length = 32)
	public String getGroupId() {
		return this.groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Column(name = "group_name", length = 200)
	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Column(name = "test_count")
	public Integer getTestCount() {
		return this.testCount;
	}

	public void setTestCount(Integer testCount) {
		this.testCount = testCount;
	}

	@Column(name = "success_count")
	public Integer getSuccessCount() {
		return this.successCount;
	}

	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}

	@Column(name = "error_count")
	public Integer getErrorCount() {
		return this.errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

	@Column(name = "operate_time", length = 19)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Timestamp getOperateTime() {
		return this.operateTime;
	}

	public void setOperateTime(Timestamp operateTime) {
		this.operateTime = operateTime;
	}

	@Column(name = "operate_end_time", length = 19)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Timestamp getOperateEndTime() {
		return this.operateEndTime;
	}

	public void setOperateEndTime(Timestamp operateEndTime) {
		this.operateEndTime = operateEndTime;
	}

	@Column(name = "continue_time")
	public Integer getContinueTime() {
		return this.continueTime;
	}

	public void setContinueTime(Integer continueTime) {
		this.continueTime = continueTime;
	}
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "gatewayServiceTestGroupLog")
	public Set<GatewayServiceTestLog> getGatewayServiceTestLogs() {
		return this.gatewayServiceTestLogs;
	}

	public void setGatewayServiceTestLogs(
			Set<GatewayServiceTestLog> gatewayServiceTestLogs) {
		this.gatewayServiceTestLogs = gatewayServiceTestLogs;
	}

}