package com.soa.database.config;


import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

@Configuration
@PropertySource(value={"classpath:config/druid.properties","classpath:config/jdbc.properties"})
public class DruidConfig {
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		DruidDataSource datasource = new DruidDataSource();
		return datasource;
	}
	
   @Bean
    public ServletRegistrationBean DruidStatViewServle(){
       //org.springframework.boot.context.embedded.ServletRegistrationBean提供类的进行注册.
	   ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(),"/druid/*");  
        //添加初始化参数：initParams
       //白名单：
       servletRegistrationBean.addInitParameter("allow","");
       //IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的话提示:Sorry, you are not permitted to view this page.
       servletRegistrationBean.addInitParameter("deny","");
       //登录查看信息的账号密码.
       servletRegistrationBean.addInitParameter("loginUsername","admin");
       servletRegistrationBean.addInitParameter("loginPassword","admin");
       return servletRegistrationBean;
    }
	 
	 @Bean  
	 public FilterRegistrationBean druidStatFilter(){  
	     FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());  
	     /** 过滤规则 */  
	     filterRegistrationBean.addUrlPatterns("/*");  
	     /** 忽略资源 */  
	     filterRegistrationBean.addInitParameter("exclusions","*.js,*.jpeg,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");  
	     return filterRegistrationBean;  
	 }  
}
