package com.soa.database.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcraft.jsch.JSchException;
import com.soa.database.entity.GatewayZuulSbaNode;
import com.soa.database.service.GatewayZuulSbaNodeService;
import com.soa.database.util.CodeMsg;
import com.soa.database.util.SSHHelper;
import com.soa.database.util.SSHResInfo;


@Controller
@RequestMapping("serviceControl")
public class GatewayZuulSbaNodeController {
	
	@Autowired
	GatewayZuulSbaNodeService gatewayZuulSbaNodeService;
	
	/**
	 * 获取所有的服务
	 * @param page
	 * @return
	 */
	@GetMapping(value="getGatewayZuulSbaNodeList")
	@ResponseBody
	public CodeMsg getGatewayZuulSbaNodeList(Integer page) {
		// TODO Auto-generated method stub
		List<GatewayZuulSbaNode> list =  gatewayZuulSbaNodeService.getGatewayZuulSbaNodeList(page);
		CodeMsg codeMsg = CodeMsg.SUCCESS;
		codeMsg.setResponse(list);
		codeMsg.setResultmessage("成功");
		return codeMsg;
	}
	
	
	/**
	 * 停止服务操作
	 * @param nodeId
	 * @return
	 */
	@PostMapping(value="stopService")
	@ResponseBody
	public CodeMsg stopService(String nodeId){
		
		GatewayZuulSbaNode gatewayZuulSbaNode  = gatewayZuulSbaNodeService.queryByPK(nodeId);
		SSHResInfo resInfo=new SSHResInfo();
		try {
			SSHHelper sSHHelper = new SSHHelper(gatewayZuulSbaNode.getIp(),null,gatewayZuulSbaNode.getUsername(),com.face.E.DecryptMessage(gatewayZuulSbaNode.getPassword(), com.face.E.sKey));
			String command = "source /etc/profile;source ~/.bash_profile;source ~/.bashrc;";
			command=command+gatewayZuulSbaNode.getShellStop();
			resInfo= sSHHelper.sendCmd(command);
			sSHHelper.close();
			if(resInfo.getExitStuts()==0){
				gatewayZuulSbaNode.setRestartTime(new Date());
				gatewayZuulSbaNode.setNodeStatus("stop");
				gatewayZuulSbaNodeService.update(gatewayZuulSbaNode);
			}
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CodeMsg codeMsg = CodeMsg.SUCCESS;
		codeMsg.setResponse(resInfo);
		codeMsg.setResultmessage("成功");
		return codeMsg;
	}
	
	/**
	 * 启动服务操作
	 * @param nodeId
	 * @return
	 */
	@PostMapping(value="startService")
	@ResponseBody
	public CodeMsg startService(String nodeId){
		GatewayZuulSbaNode gatewayZuulSbaNode  = gatewayZuulSbaNodeService.queryByPK(nodeId);
		SSHResInfo resInfo=new SSHResInfo();
		try {
			SSHHelper sSHHelper = new SSHHelper(gatewayZuulSbaNode.getIp(),null,gatewayZuulSbaNode.getUsername(),com.face.E.DecryptMessage(gatewayZuulSbaNode.getPassword(), com.face.E.sKey));
			String command = "source /etc/profile;source ~/.bash_profile;source ~/.bashrc;";
			command=command+gatewayZuulSbaNode.getShellStart();
			resInfo= sSHHelper.sendCmd(command);
			sSHHelper.close();
			if(resInfo.getExitStuts()==0){
				gatewayZuulSbaNode.setRestartTime(new Date());
				gatewayZuulSbaNode.setNodeStatus("start");
				gatewayZuulSbaNodeService.update(gatewayZuulSbaNode);
			}
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CodeMsg codeMsg = CodeMsg.SUCCESS;
		codeMsg.setResponse(resInfo);
		codeMsg.setResultmessage("成功");
		return codeMsg;
	}
	
	/**
	 * 查看服务状态
	 * @param nodeId
	 * @return
	 */
	@GetMapping(value="getServiceState")
	@ResponseBody
	public CodeMsg getServiceState(String nodeId){
		GatewayZuulSbaNode gatewayZuulSbaNode  = gatewayZuulSbaNodeService.queryByPK(nodeId);
		SSHResInfo resInfo=new SSHResInfo();
		try {
			SSHHelper sSHHelper = new SSHHelper(gatewayZuulSbaNode.getIp(),null,gatewayZuulSbaNode.getUsername(),com.face.E.DecryptMessage(gatewayZuulSbaNode.getPassword(), com.face.E.sKey));
			String command = "source /etc/profile;source ~/.bash_profile;source ~/.bashrc;";
			command=command+gatewayZuulSbaNode.getShellStatus();
			resInfo= sSHHelper.sendCmd(command);
			sSHHelper.close();
			gatewayZuulSbaNode.setNodeStatus("stop");
			if(resInfo.getOutRes().contains("LISTEN") &&resInfo.getOutRes().contains(gatewayZuulSbaNode.getPort())){
				gatewayZuulSbaNode.setNodeStatus("start");
			}
			gatewayZuulSbaNodeService.update(gatewayZuulSbaNode);
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CodeMsg codeMsg = CodeMsg.SUCCESS;
		codeMsg.setResponse(gatewayZuulSbaNode);
		codeMsg.setResultmessage("成功");
		return codeMsg;
	}

}
