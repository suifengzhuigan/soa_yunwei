package com.soa.database.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledService {
	
    @Scheduled(cron = "0/10 * * * * *")
    public void scheduled(){
    	
    }
}
