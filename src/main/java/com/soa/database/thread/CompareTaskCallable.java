package com.soa.database.thread;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;
import com.soa.database.entity.GatewayServiceTest;
import com.soa.database.entity.GatewayServiceTestLog;
import com.soa.database.util.HttpClientUtil;
import com.soa.database.util.HttpResponse;


public class CompareTaskCallable implements Callable<String>{
	Logger logger = LoggerFactory.getLogger(getClass());
    private GatewayServiceTest gatewayServiceTest;
    private  GatewayServiceTestLog gatewayServiceTestLog;
    private Map<String, String> headerMap;
    private Map<String, String> paramMap;
    public CompareTaskCallable(GatewayServiceTest gatewayServiceTest,GatewayServiceTestLog gatewayServiceTestLog,Map<String, String> paramMap,Map<String, String> headerMap){
        this.gatewayServiceTest=gatewayServiceTest;
        this.gatewayServiceTestLog=gatewayServiceTestLog;
        this.headerMap=headerMap;
        this.paramMap=paramMap;
    }
    @Override
    public String call() throws Exception {
    	Gson gson = new Gson();
    	Long allcount = 0l,successcount=0l,errocount=0l;
    	
    	Map<String, Object> resultMap = new HashMap<String, Object>();
	
			allcount++;
			
			HttpResponse result = new HttpResponse();
			long servicestarttime=System.currentTimeMillis();
			try {
				
				if ("GET".equals(gatewayServiceTest.getMethod())) {
					result = HttpClientUtil.httpGet(gatewayServiceTest.getPath(), paramMap,
							headerMap, "UTF-8");
				}
				else if ("POST".equals(gatewayServiceTest.getMethod())) {
					if ("3".equals(gatewayServiceTest.getBodyType())) {
						result = HttpClientUtil.httpPostRaw(
								gatewayServiceTest.getPath(),
								paramMap.get("json"), headerMap, "UTF-8");
					} else {
						result = HttpClientUtil.httpPostForm(
								gatewayServiceTest.getPath(),
								paramMap, headerMap, "UTF-8");
					}
				}else{
					throw new Exception("未找到请求方法，POST?GET?");
				}
			
			String jsonPathResult="";
			if(result.getStatusCode()==200){
				jsonPathResult="成功返回";
			}else{
				jsonPathResult="返回失败，返回码为:"+result.getStatusCode();
			}
			if(!StringUtils.isEmpty(gatewayServiceTest.getAssert1()) &&!StringUtils.isEmpty(gatewayServiceTest.getAssert1Value()) ){
				String obj = JsonPath.read(result.getBody(), gatewayServiceTest.getAssert1());
				if(obj!=null && obj.equals(gatewayServiceTest.getAssert1Value())){
					jsonPathResult="断言1匹配成功";
				}else{
					jsonPathResult="断言1匹配失败";
					gatewayServiceTest.setReason("断言1验证失败");
				}
			}
			if(!StringUtils.isEmpty(gatewayServiceTest.getAssert2()) &&!StringUtils.isEmpty(gatewayServiceTest.getAssert2Value()) ){
				String obj2 = JsonPath.read(result.getBody(), gatewayServiceTest.getAssert2());
				if(obj2!=null && obj2.equals(gatewayServiceTest.getAssert2Value())){
					jsonPathResult="断言2匹配成功";
				}else{
					jsonPathResult="断言2匹配失败功";
					gatewayServiceTest.setReason("断言2验证失败");
				}
			}
			if(jsonPathResult.contains("成功")){
				gatewayServiceTest.setState("1");
				gatewayServiceTest.setReason("成功");
				successcount++;
			}
			if(jsonPathResult.contains("失败")){
				gatewayServiceTest.setState("0");
				errocount++;
			}
			} catch (Exception e) {
				gatewayServiceTest.setState("0");
				gatewayServiceTest.setReason("执行请求异常:"+e.toString());
				errocount++;
				e.printStackTrace();
			}
			long serviceendtime=System.currentTimeMillis();
			gatewayServiceTestLog.setContinueTime(((Long)(serviceendtime-servicestarttime)).intValue());
			gatewayServiceTestLog.setOperateEndTime(new Timestamp(new Date().getTime()) );
			gatewayServiceTestLog.setResponseContent(result.getBody());
			gatewayServiceTestLog.setState(gatewayServiceTest.getReason());
			resultMap.put("testState", gatewayServiceTest.getState());
			resultMap.put("testReason", gatewayServiceTest.getReason());
			resultMap.put("logContinueTime", ((Long)(serviceendtime-servicestarttime)));
			resultMap.put("logOperateEndTime", new Date().getTime());
			resultMap.put("logResponseContent", result.getBody());
			resultMap.put("logstate", gatewayServiceTestLog.getState());
			resultMap.put("logid", gatewayServiceTestLog.getId());
			resultMap.put("testid", gatewayServiceTest.getId());
			resultMap.put("allcount", allcount);
	    	resultMap.put("successcount", successcount);
	    	resultMap.put("errocount", errocount);
    	return gson.toJson(resultMap);
    }
}